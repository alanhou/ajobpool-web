import React from 'react'
import { shallow } from 'enzyme'
import { Team_ } from './Team'

const teamData = {
  id: 876,
  isActive: true,
  description: 'Subway',
  url: 'www.subway.com',
  votes: [{ id: 1, user: { id: 0 } }, { id: 2, user: { id: 0 } }],
  manager: { id: 1, name: 'Han Solo' },
  createdAt: 1503354139569,
  updatedAt: 1503354139569
}

test(
  'given the user has voted the team,' +
    'the team should render a down vote button',
  () => {
    const team = shallow(
      <Team_ key={teamData.id} index={2} team={teamData} userId={0} />
    )
    expect(team).toMatchSnapshot()
  }
)

test(
  'given the user has not voted the team,' +
    'the team should render an up vote button',
  () => {
    const team = shallow(
      <Team_ key={teamData.id} index={2} team={teamData} userId={3} />
    )
    expect(team).toMatchSnapshot()
  }
)

test(
  'given the user is the manager of the team,' +
    'the team should render an edit button',
  () => {
    const team = shallow(
      <Team_ key={teamData.id} index={2} team={teamData} userId={1} />
    )
    expect(team).toMatchSnapshot()
  }
)

test(
  'given the user is not the manager of the team,' +
    'the team should not render an edit button',
  () => {
    const team = shallow(
      <Team_ key={teamData.id} index={2} team={teamData} userId={2} />
    )
    expect(team).toMatchSnapshot()
  }
)

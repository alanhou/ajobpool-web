import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import { Header } from './Header'
import { TeamList } from './TeamList'
import { NewTeam } from './NewTeam'
import { SignIn } from './SignIn'
import { TeamSearch } from './TeamSearch'

const styles = {
  app: { textAlign: 'center' },
  appHeader: {
    padding: 20,
    backgroundColor: '#222',
    color: 'white'
  }
}

export class App extends Component {
  render () {
    return (
      <div style={styles.app}>
        <div style={styles.appHeader}>
          <h2>Welcome to Ajobpool</h2>
        </div>
        <Header />
        <div className='ph3 pv1 background-gray'>
          <Switch>
            <Route exact path='/' component={TeamList} />
            <Route exact path='/sign-in' component={SignIn} />
            <Route exact path='/teams/new' component={NewTeam} />
            <Route exact path='/teams/search' component={TeamSearch} />
          </Switch>
        </div>
      </div>
    )
  }
}

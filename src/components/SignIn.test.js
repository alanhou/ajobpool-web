import React from 'react'
import { shallow } from 'enzyme'
import { SignIn_ } from './SignIn'

test('it should render the team', () => {
  const signIn = shallow(<SignIn_ />)
  expect(signIn).toMatchSnapshot()
})

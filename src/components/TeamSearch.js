import React, { Component } from 'react'
import { withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import { Team } from './Team'

export class TeamSearch_ extends Component {
  state = {
    teams: [],
    searchText: '',
    location: '',
    distance: 10
  }

  render () {
    return (
      <div>
        <div>
          <label>
            What:
            <input
              type='text'
              value={this.state.searchText}
              onChange={event =>
                this.setState({ searchText: event.target.value })}
            />
          </label>
          <br />
          <label>
            Where:
            <input
              type='text'
              value={this.state.location}
              onChange={event =>
                this.setState({ location: event.target.value })}
            />
          </label>
          <br />
          <label>
            Distance:
            <select
              value={this.state.distance}
              onChange={event =>
                this.setState({ distance: event.target.value })}
            >
              <option value={10}>10 miles</option>
              <option value={20}>20 miles</option>
              <option value={50}>50 miles</option>
              <option value={1000}>100 miles</option>
            </select>
          </label>
          <br />
          <button onClick={() => this.searchForTeams()}>Search</button>
        </div>
        {this.state.teams.map((team, index) =>
          <Team key={team.id} team={team} index={index} />
        )}
      </div>
    )
  }

  searchForTeams = async () => {
    const result = await this.props.client.query({
      query: SEARCH_TEAMS_QUERY,
      variables: { searchText: this.state.searchText }
    })
    this.setState({ teams: result.data.allTeams })
  }
}

const SEARCH_TEAMS_QUERY = gql`
  query SearchTeams($searchText: String!) {
    allTeams(
      filter: {
        OR: [
          { url_contains: $searchText }
          { description_contains: $searchText }
        ]
      }
    ) {
      id
      url
      description
      createdAt
      manager {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`
export const TeamSearch = withApollo(TeamSearch_)

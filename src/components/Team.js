import React, { Component } from 'react'
import moment from 'moment'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import { READ_TEAMS } from './TeamList'

export class Team_ extends Component {
  state = {
    isUpdating: false,
    isActive: this.props.team.isActive,
    description: this.props.team.description,
    url: this.props.team.url
  }

  userIsTheManagerOfTheTeam = () => {
    return this.props.userId === this.props.team.manager.id
  }

  userHasVotedTheTeam = () => {
    return this.props.team.votes
      .map(vote => vote.user.id)
      .includes(this.props.userId)
  }

  upVote = async () => {
    if (!this.props.userId) {
      this.props.history.push(`/sign-in`)
      return
    }
    await this.props.createVote({
      variables: {
        userId: this.props.userId,
        teamId: this.props.team.id
      },
      update: (store, { data: { createVote } }) => {
        this.props.updateStoreAfterVote(store, createVote, this.props.team.id)
      }
    })
  }

  downVote = async () => {
    await this.props.deleteVote({
      variables: {
        voteId: this.props.team.votes.find(
          vote => vote.user.id === this.props.userId
        ).id
      },
      update: (store, { data: { deleteVote } }) => {
        const data = store.readQuery({ query: READ_TEAMS })
        data.allTeams.find(
          team => team.id === this.props.team.id
        ).votes = this.props.team.votes.filter(
          vote => vote.id !== deleteVote.id
        )
        store.writeQuery({ query: READ_TEAMS, data })
      }
    })
  }

  saveTeam = async () => {
    await this.props.updateTeam({
      variables: {
        teamId: this.props.team.id,
        description: this.state.description,
        url: this.state.url,
        isActive: this.state.isActive
      },
      update: (store, { data: { updateTeam } }) => {
        const data = store.readQuery({ query: READ_TEAMS })
        data.allTeams[data.allTeams.indexOf(this.props.team)] = updateTeam
        store.writeQuery({ query: READ_TEAMS, data })
      }
    })
    this.setState({ isUpdating: false })
  }

  removeTeam = async () => {
    await this.props.deleteTeam({
      variables: {
        teamId: this.props.team.id
      },
      update: (store, { data: { deleteTeam } }) => {
        const data = store.readQuery({ query: READ_TEAMS })
        data.allTeams = data.allTeams.filter(team => team.id !== deleteTeam.id)
        store.writeQuery({ query: READ_TEAMS, data })
      }
    })
  }

  render () {
    if (!this.state.isUpdating) {
      return (
        <div>
          <span>{this.props.index + 1}</span>
          <div>Status: {this.props.team.isActive ? 'Active' : 'Inactive'}</div>
          <div>Description: {this.props.team.description}</div>
          URL: <a href={this.props.team.url}>{this.props.team.url}</a>
          <div>Manager: {this.props.team.manager.name}</div>
          <div>Created at: {moment(this.props.team.createdAt).calendar()}</div>
          <div>Updated at: {moment(this.props.team.updatedAt).fromNow()}</div>
          <div>Votes: {this.props.team.votes.length}</div>
          {this.userHasVotedTheTeam()
            ? <button onClick={() => this.downVote()}>Down vote</button>
            : <button onClick={() => this.upVote()}>Up vote</button>}
          {this.userIsTheManagerOfTheTeam() &&
            <button onClick={() => this.setState({ isUpdating: true })}>
              Edit
            </button>}
          {this.userIsTheManagerOfTheTeam() &&
            <button onClick={() => this.removeTeam()}>Remove</button>}
        </div>
      )
    } else {
      return (
        <div>
          <label>
            <input
              type='checkbox'
              checked={this.state.isActive}
              onChange={event =>
                this.setState({ isActive: event.target.checked })}
            />
            Active
          </label>
          <br />
          <label>
            Description:
            <input
              type='text'
              value={this.state.description}
              onChange={event =>
                this.setState({ description: event.target.value })}
            />
          </label>
          <br />
          <label>
            URL:
            <input
              type='text'
              value={this.state.url}
              onChange={event => this.setState({ url: event.target.value })}
            />
          </label>
          <br />
          <button onClick={() => this.setState({ isUpdating: false })}>
            Cancel
          </button>
          <button onClick={() => this.saveTeam()}>Save</button>
        </div>
      )
    }
  }
}

const UPDATE_TEAM = gql`
  mutation UpdateTeam(
    $teamId: ID!
    $isActive: Boolean
    $description: String!
    $url: String!
  ) {
    updateTeam(
      id: $teamId
      isActive: $isActive
      description: $description
      url: $url
    ) {
      id
      createdAt
      updatedAt
      isActive
      description
      url
      manager {
        id
        name
      }
    }
  }
`

const DELETE_TEAM = gql`
  mutation DeleteTeam($teamId: ID!) {
    deleteTeam(id: $teamId) {
      id
    }
  }
`

const CREATE_VOTE = gql`
  mutation CreateVote($userId: ID!, $teamId: ID!) {
    createVote(userId: $userId, teamId: $teamId) {
      id
      team {
        votes {
          id
          user {
            id
          }
        }
      }
      user {
        id
      }
    }
  }
`

const DELETE_VOTE = gql`
  mutation DeleteVote($voteId: ID!) {
    deleteVote(id: $voteId) {
      id
    }
  }
`

export const Team = compose(
  graphql(UPDATE_TEAM, { name: 'updateTeam' }),
  graphql(DELETE_TEAM, { name: 'deleteTeam' }),
  graphql(CREATE_VOTE, { name: 'createVote' }),
  graphql(DELETE_VOTE, { name: 'deleteVote' })
)(Team_)

import React from 'react'
import { shallow } from 'enzyme'
import { TeamSearch_ } from './TeamSearch'

test('it should render all teams', () => {
  const teamSearch = shallow(
    <TeamSearch_
      data={{
        allTeams: [
          { id: '1', description: 'Red Bento' },
          { id: '2', description: 'Subway' }
        ]
      }}
    />
  )
  expect(teamSearch).toMatchSnapshot()
})

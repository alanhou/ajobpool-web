import React from 'react'
import { shallow } from 'enzyme'
import { Header_ } from './Header'

test(
  'given the user is not signed in,' +
    'it should render the header with a sign in button',
  () => {
    global.localStorage.getItem = jest.fn(item => null)
    const header = shallow(<Header_ />)
    expect(header).toMatchSnapshot()
  }
)

test(
  'given the user is signed in,' +
    'it should render the header with a sign off button',
  () => {
    global.localStorage.getItem = jest.fn(item => item)
    const header = shallow(<Header_ />)
    expect(header).toMatchSnapshot()
  }
)

import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { GC_USER_ID } from '../constants'
import { READ_TEAMS } from './TeamList'

export class NewTeam_ extends Component {
  state = {
    description: '',
    url: ''
  }

  createTeam = async () => {
    const userId = global.localStorage.getItem(GC_USER_ID)
    if (!userId) {
      this.props.history.push(`/sign-in`)
      return
    }
    const { description, url } = this.state
    await this.props.createTeam({
      variables: {
        description,
        url,
        userId
      },
      update: (store, { data: { createTeam } }) => {
        const data = store.readQuery({ query: READ_TEAMS })
        data.allTeams.splice(0, 0, createTeam)
        store.writeQuery({ query: READ_TEAMS, data })
      }
    })
    this.props.history.push(`/`)
  }

  render () {
    return (
      <div>
        <label>
          Team name:
          <input
            type='text'
            value={this.state.description}
            onChange={event =>
              this.setState({ description: event.target.value })}
          />
        </label>
        <br />
        <label>
          Team website:
          <input
            type='text'
            value={this.state.url}
            onChange={event => this.setState({ url: event.target.value })}
          />
        </label>
        <br />
        <button onClick={() => this.createTeam()}>Create team</button>
      </div>
    )
  }
}

const CREATE_TEAM_MUTATION = gql`
  mutation CreateTeam($description: String!, $url: String!, $userId: ID!) {
    createTeam(description: $description, url: $url, managerId: $userId) {
      id
      isActive
      createdAt
      updatedAt
      url
      description
      manager {
        id
        name
      }
      votes {
        id
      }
    }
  }
`

export const NewTeam = graphql(CREATE_TEAM_MUTATION, { name: 'createTeam' })(
  NewTeam_
)

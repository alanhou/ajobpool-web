import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Team } from './Team'
import { GC_USER_ID } from '../constants'

export class TeamList_ extends Component {
  componentDidMount () {
    // this._subscribeToNewTeams()
    // this._subscribeToNewVotes()
  }

  updateCacheAfterVote = (store, createVote, teamId) => {
    const data = store.readQuery({ query: READ_TEAMS })
    const votedTeam = data.allTeams.find(team => team.id === teamId)
    votedTeam.votes = createVote.team.votes
    store.writeQuery({ query: READ_TEAMS, data })
  }

  render () {
    if (this.props.data && this.props.data.loading) {
      return <div>Loading</div>
    }

    if (this.props.data && this.props.data.error) {
      return <div>Error</div>
    }
    const teams = this.props.data.allTeams
    return (
      <div>
        {teams.map((team, index) =>
          <Team
            key={team.id}
            index={index}
            team={team}
            history={this.props.history}
            userId={global.localStorage.getItem(GC_USER_ID)}
            updateStoreAfterVote={this.updateCacheAfterVote}
          />
        )}
      </div>
    )
  }

  // _subscribeToNewVotes = () => {
  //   this.props.data.subscribeToMore({
  //     document: gql`
  //       subscription {
  //         Vote(filter: { mutation_in: [CREATED, UPDATED, DELETED] }) {
  //           node {
  //             id
  //             team {
  //               id
  //               url
  //               description
  //               createdAt
  //               manager {
  //                 id
  //                 name
  //               }
  //               votes {
  //                 id
  //                 user {
  //                   id
  //                 }
  //               }
  //             }
  //             user {
  //               id
  //             }
  //           }
  //         }
  //       }
  //     `,
  //     updateQuery: (previous, { subscriptionData }) => {
  //       const votedTeamIndex = previous.allTeams.findIndex(
  //         team => team.id === subscriptionData.data.Vote.node.team.id
  //       )
  //       const team = subscriptionData.data.Vote.node.team
  //       const newAllTeams = previous.allTeams.slice()
  //       newAllTeams[votedTeamIndex] = team
  //       return {
  //         ...previous,
  //         allTeams: newAllTeams
  //       }
  //     }
  //   })
  // }

  // _subscribeToNewTeams = () => {
  //   this.props.data.subscribeToMore({
  //     document: gql`
  //       subscription {
  //         Team(filter: { mutation_in: [CREATED] }) {
  //           node {
  //             id
  //             url
  //             description
  //             createdAt
  //             manager {
  //               id
  //               name
  //             }
  //             votes {
  //               id
  //               user {
  //                 id
  //               }
  //             }
  //           }
  //         }
  //       }
  //     `,
  //     updateQuery: (previous, { subscriptionData }) => {
  //       const newAllTeams = [
  //         subscriptionData.data.Team.node,
  //         ...previous.allTeams
  //       ]
  //       return {
  //         ...previous,
  //         allTeams: newAllTeams
  //       }
  //     }
  //   })
  // }
}

export const READ_TEAMS = gql`
  query ReadTeams {
    allTeams {
      id
      isActive
      createdAt
      updatedAt
      url
      description
      manager {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`

export const TeamList = graphql(READ_TEAMS)(TeamList_)

import React from 'react'
import { shallow } from 'enzyme'
import { TeamList_ } from './TeamList'

test('it should render all teams', () => {
  const teamList = shallow(
    <TeamList_
      data={{
        allTeams: [
          { id: '1', description: 'Red Bento' },
          { id: '2', description: 'Subway' }
        ]
      }}
    />
  )
  expect(teamList).toMatchSnapshot()
})

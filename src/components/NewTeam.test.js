import React from 'react'
import { shallow } from 'enzyme'
import { NewTeam_ } from './NewTeam'

test('it should render the team', () => {
  const newTeam = shallow(<NewTeam_ />)
  expect(newTeam).toMatchSnapshot()
})

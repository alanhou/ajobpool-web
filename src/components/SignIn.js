import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants'

export class SignIn_ extends Component {
  state = {
    signIn: true, // switch between SignIn_ and SignUp
    email: '',
    password: '',
    name: ''
  }

  confirm = async () => {
    const { name, email, password } = this.state
    if (this.state.signIn) {
      const result = await this.props.authenticateUser({
        variables: {
          email,
          password
        }
      })
      const { id, token } = result.data.authenticateUser
      this.saveUserData(id, token)
    } else {
      const result = await this.props.signupUser({
        variables: {
          name,
          email,
          password
        }
      })
      const { id, token } = result.data.signupUser
      this.saveUserData(id, token)
    }
    this.props.history.push(`/`)
  }

  saveUserData = (id, token) => {
    global.localStorage.setItem(GC_USER_ID, id)
    global.localStorage.setItem(GC_AUTH_TOKEN, token)
  }

  render () {
    return (
      <div>
        <div className='flex flex-column'>
          {!this.state.signIn &&
            <input
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
              type='text'
              placeholder='Your name'
            />}
          <input
            value={this.state.email}
            onChange={e => this.setState({ email: e.target.value })}
            type='text'
            placeholder='Your email address'
          />
          <input
            value={this.state.password}
            onChange={e => this.setState({ password: e.target.value })}
            type='password'
            placeholder='Choose a safe password'
          />
        </div>
        <div className='flex mt3'>
          <div className='pointer mr2 button' onClick={() => this.confirm()}>
            {this.state.signIn ? 'Sign in' : 'Create account'}
          </div>
          <div
            className='pointer button'
            onClick={() => this.setState({ signIn: !this.state.signIn })}
          >
            {this.state.signIn
              ? 'need to create an account?'
              : 'already have an account?'}
          </div>
        </div>
      </div>
    )
  }
}

const SIGNUP_USER_MUTATION = gql`
  mutation SignupUserMutation(
    $email: String!
    $password: String!
    $name: String!
  ) {
    signupUser(email: $email, password: $password, name: $name) {
      id
      token
    }
  }
`

const AUTHENTICATE_USER_MUTATION = gql`
  mutation AuthenticateUserMutation($email: String!, $password: String!) {
    authenticateUser(email: $email, password: $password) {
      token
      id
    }
  }
`

export const SignIn = compose(
  graphql(SIGNUP_USER_MUTATION, { name: 'signupUser' }),
  graphql(AUTHENTICATE_USER_MUTATION, { name: 'authenticateUser' })
)(SignIn_)

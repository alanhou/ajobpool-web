import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants'

export class Header_ extends Component {
  signOut = () => {
    global.localStorage.removeItem(GC_USER_ID)
    global.localStorage.removeItem(GC_AUTH_TOKEN)
    this.props.history.push(`/`)
  }

  render () {
    const userId = global.localStorage.getItem(GC_USER_ID)
    return (
      <div className='flex pa1 justify-between nowrap orange'>
        <div className='flex flex-fixed black'>
          <Link to='/' className='ml1 no-underline black'>
            Home
          </Link>
          <div className='ml1'>|</div>
          <Link to='/teams/search' className='ml1 no-underline black'>
            Find teams
          </Link>
          <div className='ml1'>|</div>
          <Link to='/teams/new' className='ml1 no-underline black'>
            New team
          </Link>
        </div>
        <div className='flex flex-fixed'>
          {userId
            ? <button
              className='ml1 pointer black'
              onClick={() => this.signOut()}
              >
                Sign out
              </button>
            : <Link to='/sign-in' className='ml1 no-underline black'>
                Sign in
              </Link>}
        </div>
      </div>
    )
  }
}

export const Header = withRouter(Header_)

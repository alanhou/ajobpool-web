## introduction
We begin with a college town that needs a new type of job board for its college students and local businesses.
The students need to get part time jobs in local businesses that fits their class schedules.
And the local businesses need to fill their jobs with students that meet the demand of their job schedules.  
Her is a concept lookup table with examples:

| concept | examples |
| --- | --- |
| team | a bar, a restaurant, a clothes store |
| supervisor | the manager of a bar, a restaurant, a clothes store |
| worker | a full time student who works part time in a team |
| job/worker type | bartender, waiter, retail salesperson |
| shift schedule | Monday morning, Wednesday afternoon, Friday night |
| skill | selling clothes, attention to details, javascript |

## problem
- given: the schedules of all workers (the supply) and the schedules of all jobs (the demand)
- task: how to map workers to jobs so the supply meets the demand

## old solutions
- supervisors post jobs on job boards such as indeed and Craig's list or a blackboard in front of the store
- workers manually send emails, submit resumes, fill out application forms and negotiate shi 
- supervisors update the job scheduling upon hiring and exiting 

## new solution
- supervisors declare jobs with specified skills and schedules
- allow workers to declare their skills and schedules
- the app match workers to jobs for interviews and updates schedules upon hiring and existing

## why is the new solution better
- no emails, no applications, no resumes, no wasting time on shift scheduling - the app does them all

## key feature
```gherkin
Feature: job-worker match
    As a supervisor
    I need to find workers that are good matches for my jobs
    So that I can interview the best candidates and fill my jobs with quickly
    
    Scenario Outline: supervisor declares jobs
      Given I declared a job with certain <job type>, <job schedule> in my team
      When a worker declared his <worker type>, <worker schedule> in his profile
      Then I should get the corresponding <match>
      Examples:
      | job type | job schedule | worker type | worker schedule | match |
      | bartender | Saturday night | bartender | Saturday night | yes |
      | bartender | Saturday night | bartender | Saturday morning | no |
      | bartender | Saturday night | cook | Saturday night | no |
      | bartender | Sunday night | bartender | Saturday night | no |
      | cook | Saturday night | bartender | Saturday night | no |
```

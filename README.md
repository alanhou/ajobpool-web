## Development Environment
To set up the development environment, install the following software:
- [the latest Node](https://nodejs.org/en/download/package-manager/)
- [the latest Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [a good Javascript editor](https://www.slant.co/topics/1686/~javascript-ides-or-editors)
- [an editor plugin for Babel](https://babeljs.io/docs/editors)
- [an editor plugin for Standard](https://standardjs.com/#are-there-text-editor-plugins)
- [an editor plugin for Prettier](https://github.com/prettier/prettier#editor-integration)

## Development Scripts
To run a common development script, execute one of the following commands:
- `yarn install`: Installs dependencies and dev dependencies.
- `yarn lint-fix`: Lints the code and tries to fix format and lint errors.
- `yarn test`: Launches the test runner in the interactive watch mode.
- `yarn start`: Builds the app in development mode and Launches the app.

## References
To learn about the technologies this project uses, visit the following websites:
- [material-ui](http://www.material-ui.com/#/)
- [create-react-app](https://github.com/facebookincubator/create-react-app)
- [development guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md)
